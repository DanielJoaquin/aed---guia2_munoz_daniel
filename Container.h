#include <iostream>

using namespace std;

#ifndef CONTAINER_H
#define CONTAINER_H

class Container{
  private:
    string nombre;
    int numero;

  public:
    /*Constructor*/
    Container();
    /*Metodos*/
    string get_nombre();
    int get_numero();

    void set_nombre(string nombre);
    void set_numero(int numero);
};
#endif
