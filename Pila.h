#include <iostream>
#include "Container.h"
using namespace std;

#ifndef PILA_H
#define PILA_H


class Pila{
  private:
    int max = 0;
    int tope = -1;
    bool band;

    Container *arreglo = NULL;

  public:
    /*Constructor*/
    Pila();
    /*Metodos*/
    void iniciar(int max);
    void pilaLlena();
    void pilaVacia();
    bool get_band();
    void push_container(Container container);
    Container pop_Container();
    void imprimier_Container(int x);
};
#endif
