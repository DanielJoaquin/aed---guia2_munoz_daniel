#include <iostream>
#include "Pila.h"

using namespace std;

/*Menu del programa*/
int Menu(){
  string opcion;
  cout << "\n-------Menu de Opciones-------" << endl;
  cout << "|[1] Agregar Container       |" << endl;
  cout << "|[2] Remover Container       |" << endl;
  cout << "|[3] Ver Puerto Seco         |" << endl;
  cout << "|[0] Salir                   |" << endl;
  cout << "------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);
  return stoi(opcion);
}

void ImprimirPuerto(int columnas, Pila *puerto, int tamano){
  cout << "---------------------- PUERTO SECO --------------------------" << endl;
	cout << "-------------------------------------------------------------" <<endl;

  for (int i = (tamano-1);i>=0; i--){
    for(int j=0; j<columnas; j++){
      puerto[j].imprimier_Container(i);
    }
    cout << endl;
  }

  for(int i=0; i<columnas; i++){
    cout << "-----------------------";
  }
  cout << endl;

  for (int i=0; i<columnas; i++){
      cout << "  Columna " << i+1 << "  ";
  }
  cout << endl;

  for(int i=0; i<columnas; i++){
    cout << "-----------------------";
  }
  cout << endl;
}

int main(){

  /*Variables*/
  string line;
  int opcion = -1;
  int tamano=0;
  int columnas=0;

  int numeroIngreso;
  string nombreEmpresa;
  int numeroContainer;

  Container container = Container();

  cout << "Cantidad de Columnas de Containers: ";
  getline(cin, line);
  columnas = stoi(line);
  Pila *puerto = new Pila[columnas];

  cout << "Ingrese numero maximo de Containers apilables: ";
  getline(cin, line);
  tamano = stoi(line);

  for(int i=0; i<columnas; i++){
    puerto[i].iniciar(tamano);
  }

  while(opcion!=0){
    opcion = Menu();
    cout << "-------------------------------------------------------------" << endl;
    switch(opcion){
      case 0:
        cout << "FIN DEL PROGRAMA" << endl;
        break;
      case 1:

        ImprimirPuerto(columnas, puerto, tamano);
        /*Ingreso de columna a agregar*/
        cout << "Donde desea agregar el COntainer" << endl;
        cout << "Numero Columna: ";
        getline(cin, line);
        numeroIngreso = stoi(line) - 1;
        /*Ingreso nombre empresa*/
        cout << "Nombre Empresa: ";
        getline(cin, line);
        nombreEmpresa = line;

        container.set_nombre(nombreEmpresa);
        /*Ingreso numero COntainer*/
        cout << "Numero Container: ";
        getline(cin, line);
        numeroContainer = stoi(line);

        container.set_numero(numeroContainer);

        puerto[numeroIngreso].push_container(container);
        break;

      case 2:
        ImprimirPuerto(columnas, puerto, tamano);
        cout << "Ingrese datos del COntainer A extraer" << endl;

        cout << "Numero Columna: ";
        getline(cin, line);
        numeroIngreso = stoi(line) - 1;

        cout << "Nombre Empresa: ";
        getline(cin, line);
        nombreEmpresa = line;

        cout << "Numero Container: ";
        getline(cin, line);
        numeroContainer = stoi(line);

        while(true){
          container = puerto[numeroIngreso].pop_Container();

          if(container.get_nombre().compare(nombreEmpresa) == 0){
            if(container.get_numero() == numeroContainer){
              ImprimirPuerto(columnas, puerto, tamano);
              break;
            }
          }
          else{
            for(int i=0; i<columnas; i++){
              if(i!=numeroIngreso){
                puerto[i].pilaLlena();
                if(puerto[i].get_band() == false){
                  puerto[i].push_container(container);

                  ImprimirPuerto(columnas, puerto, tamano);
                  break;
                }
              }
            }
          }
        }
        break;

      case 3:
        ImprimirPuerto(columnas, puerto, tamano);
        break;
    }
  }



  return 0;
}
