#include <iostream>
#include "Pila.h"

using namespace std;

/*Constructor*/
Pila::Pila(){

}

/*Metodos*/
void Pila::iniciar(int max){
  this->max = max -1;
  this->arreglo = new Container[max];
}

void Pila::pilaLlena(){/*Verificador si la pila esta o no llena*/
  if(this->tope == max){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

void Pila::pilaVacia(){ /*Verificador si la pila esta o no vacia*/
  if(this->tope == -1){
    this->band = true;
  }
  else{
    this->band = false;
  }
}

bool Pila::get_band(){
  return this->band;
}

void Pila::push_container(Container container){
  pilaLlena();

  if(band == false){
    this->tope = this->tope + 1;
    arreglo[this->tope] = container;
    cout << "Adicion exitosa de Container" << endl;
  }
  else{
    cout << "No se pudo agregar en esta columna" << endl;
  }
}

Container Pila::pop_Container(){
  pilaVacia();
  Container temporal = Container();

  if(band == false){
    temporal = arreglo[this->tope];
    arreglo[this->tope].set_nombre("Disponible");
    arreglo[this->tope].set_numero(0);
    this->tope = this->tope - 1;
    cout << "Extraccion exitosa" << endl;
    return temporal;
  }
  else{
    cout << "Columna vacia" << endl;
  }
}

void Pila::imprimier_Container(int x){
  cout << "|" << arreglo[x].get_nombre();
  if(arreglo[x].get_numero()!=0){
    cout << "/" << arreglo[x].get_numero();
  }
  else{
    cout << "|" << endl;
  }
}
